#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==========================================================================================
## @file chic_turbo.py
#
#  chi_c -> J/psi mu+ mu-
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date   2017-06-17
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#
# =============================================================================
""" chic -> J/psi mu+ mu-

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
    ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
    ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = 'Vanya BELYAEV  Ivan.Belyaev@itep.ru'
__date__    = "2017-06-17"
__version__ = '$Revision$' 
# =============================================================================
import ROOT                           ## needed to produce/visualize the histograms
from   Bender.Main         import *   ## import all bender goodies 
import LHCbMath.Types                 ## easy access to various geometry routines 
from   Gaudi.Configuration import *   ## needed for job configuration
# =============================================================================
from   GaudiKernel.SystemOfUnits     import GeV, MeV, mm 
from   GaudiKernel.PhysicalConstants import c_light
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger 
logger = getLogger( __name__ )
# =============================================================================
import BenderTools.TisTos ## add methods for TisTos 
import BenderTools.Fill   ## add methods to fill n-tuple info 
# =============================================================================
## @class ChiC
#  Simple algorithm to study chi_c -> J/psi mu+ mu-
#  @date   2011-05-27
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
class ChiC( Algo ) :
    """
    Simple algorithm to study chi_c -> J/psi mu+ mu-
    """
    def initialize ( self ) :
        
        sc = Algo.       initialize ( self )
        if sc.isFailure() : return sc
        
        triggers = {}
        lines    = {}
        lines [ "psi"  ] = {}
        lines [ "psi1" ] = {}
        lines [ "psi2" ] = {}
        lines [ "psi3" ] = {}
        lines [ "psi4" ] = {}
        #    
        ## J/psi
        #
        lines [ "psi" ][   'L0TOS' ] = 'L0(DiMuon|Muon)Decision'
        lines [ "psi" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
        lines [ "psi" ][ 'Hlt1TOS' ] = 'Hlt1(DiMuon|SingleMuon(?!NoIP)|TrackMuon).*Decision'
        lines [ "psi" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
        lines [ "psi" ][ 'Hlt2TOS' ] = 'Hlt2(DiMuon|SingleMuon).*Decision'
        lines [ "psi" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'
        #
        ## J/psi ("clean-1")
        #
        lines [ "psi1" ][   'L0TOS' ] = 'L0(DiMuon|Muon)Decision'
        lines [ "psi1" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
        lines [ "psi1" ][ 'Hlt1TOS' ] = 'Hlt1(DiMuon|TrackMuon).*Decision'
        lines [ "psi1" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
        lines [ "psi1" ][ 'Hlt2TOS' ] = 'Hlt2DiMuon.*Decision'
        lines [ "psi1" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'
        #
        ## J/psi ("TURBO")
        #
        lines [ "psi2" ][   'L0TOS' ] = 'L0DiMuonDecision'
        lines [ "psi2" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
        lines [ "psi2" ][ 'Hlt1TOS' ] = 'Hlt1DiMuonHighMass.*Decision'
        lines [ "psi2" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
        lines [ "psi2" ][ 'Hlt2TOS' ] = 'Hlt2DiMuonJPsiTurboDecision'
        lines [ "psi2" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'
        #
        ## J/psi ("unbiased") 
        #
        lines [ "psi3" ][   'L0TOS' ] = 'L0(DiMuon|Muon)Decision'
        lines [ "psi3" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
        lines [ "psi3" ][ 'Hlt1TOS' ] = 'Hlt1DiMuonHighMass.*Decision'
        lines [ "psi3" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
        lines [ "psi3" ][ 'Hlt2TOS' ] = 'Hlt2DiMuonJPsiHighPT.*Decision'
        lines [ "psi3" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'

        sc = self.tisTos_initialize ( triggers , lines )
        if sc.isFailure () : return sc
        
        self.l2tistos = None 
        
        self.mfit_1     = DTF_FUN      ( M   , True  , strings(['J/psi(1S)']) ) / GeV
        self.mfit_2     = DTF_FUN      ( M   , False , strings(['J/psi(1S)']) ) / GeV
        self.m23_1      = DTF_FUN      ( M23 , True  , strings(['J/psi(1S)']) ) / GeV
        self.m23_2      = DTF_FUN      ( M23 , False , strings(['J/psi(1S)']) ) / GeV
        self.c2dtf_1    = DTF_CHI2NDOF (      True  )
        self.c2dtf_2    = DTF_CHI2NDOF (      False )
        self.tz         = BPVTZ        ()
        
        return SUCCESS 
    
    ## the only one esential method: 
    def analyse  ( self ) :
        """
        The only one essential method
        """
        
        # 
        ## rec summary
        #
        ## rc_summary   = self.get( '/Event/Rec/Summary' ).summaryData()
        odin         = self.get( '/Event/DAQ/ODIN'    )
        #
        
        chics = self.select ( 'chic'  , 'Meson -> ( J/psi(1S) -> mu+ mu- ) mu+ mu-' )
        if not chics : self.Warning ( 'No chic are found!' ,SUCCESS )
        
        tup    = self.nTuple ( 'chic' )
        
        for chi in chics  :

            psi     = chi(1)
            
            mu1     = chi(2) ## 1st muon from chi 
            mu2     = chi(3) ## 2nd muon from chi 
            
            mu1_psi = psi(1) ## 1st muon from psi 
            mu2_psi = psi(2) ## 2nd muon from psi 

            self.treatKine   ( tup , psi  , '_psi' ) ## general kinematic
            self.treatKine   ( tup , chi  , '_chi' ) ## general kinematic 
            
            tup.column_float ( 'mass_1'     , self.mfit_1  ( chi ) ) ## with PV-constraint
            tup.column_float ( 'mass_2'     , self.mfit_2  ( chi ) ) ##   no PV-constraint
            tup.column_float ( 'm23_1'      , self.m23_1   ( chi ) ) ## with PV-constraint
            tup.column_float ( 'm23_2'      , self.m23_2   ( chi ) ) ##   no PV-constraint
            tup.column_float ( 'c2dtf_1'    , self.c2dtf_1 ( chi ) ) ## with PV-constraint 
            tup.column_float ( 'c2dtf_2'    , self.c2dtf_2 ( chi ) ) ##   no PV-constraint

            tup.column_float ( 'tz_psi' , self.tz ( psi ) )
            tup.column_float ( 'tz_chi' , self.tz ( chi ) )
            
            ## add the information needed for TisTos
            self.tisTos ( psi , tup , 'psi_0_' , self.lines [ 'psi'  ] , self.l0tistos , self.l1tistos , self.l2tistos )
            
            ## ## add the information needed for TisTos
            ## self.tisTos ( psi , tup , 'psi_1_' , self.lines [ 'psi1' ] , self.l0tistos , self.l1tistos , self.l2tistos )
            
            ## add the information needed for TisTos
            self.tisTos ( psi , tup , 'psi_2_' , self.lines [ 'psi2' ] , self.l0tistos , self.l1tistos , self.l2tistos )
            
            ## add the information needed for TisTos
            self.tisTos ( psi , tup , 'psi_3_' , self.lines [ 'psi3' ] , self.l0tistos , self.l1tistos , self.l2tistos )

            ## add the information for PID efficiency correction
            self.treatMuons    ( tup , chi ) 
            
            ## add the information needed for track efficiency correction
            self.treatTracks   ( tup , chi )
            
            ## add some reco-summary information 
            ## self.addRecSummary ( tup , rc_summary   )

            ## ODIN
            tup .column_aux    ( odin )
            
            tup.write()
            
        return SUCCESS 

# =============================================================================
## configure the job 
def configure ( datafiles        ,
                catalogs = []    ,
                castor   = False ,
                params   = {}    ) :
    """
    Job configuration 
    """
    
    from Configurables      import DaVinci       ## needed for job configuration
    
    from BenderTools.Parser import theYear, hasInFile 
    the_year  = theYear ( datafiles , params , '2016' )
    
    #
    ## check
    #
    if '2011' == the_year and hasInFile ( datafiles , 'Collision12' ) :
        raise AttributeError, 'Invalid Year %s ' % the_year
    if '2012' == the_year and hasInFile ( datafiles , 'Collision11' ) :
        raise AttributeError, 'Invalid Year %s ' % the_year 
    
    logger.info ( 'Use the Year = %s ' % the_year )
    
    from Configurables import LHCbApp
    LHCbApp().XMLSummary = 'summary.xml'

    from PhysConf.Filters import LoKi_Filters
    fltrs = LoKi_Filters (
        HLT2_Code = """
        HLT_PASS_RE('.*Hlt2DiMuonJPsiTurbo.*')
        """
        )

    davinci = DaVinci (
        #
        EventPreFilters = fltrs.filters('Fltrs') ,
        DataType        = the_year         ,
        InputType       = 'MDST'           ,
        RootInTES       = '/Event/Turbo'   ,
        Simulation      = False            ,
        PrintFreq       = 2000             ,
        EvtMax          = -1               ,
        #
        HistogramFile   = 'PsiMuMu_Histos.root' ,
        TupleFile       = 'PsiMuMu.root'        ,
        ##
        Lumi            = True ,
        ##
        Turbo           = True   ##  important 
        )
    
    from PhysConf.Selections import AutomaticData
    psi = AutomaticData('Hlt2DiMuonJPsiTurbo/Particles')
    
    if '2015' == the_year : 
        from PhysConf.Selections import MomentumScaling
        psi = MomentumScaling ( psi , Year = the_year , Turbo = 'PERSISTRECO' )

    from PhysConf.Selections import FilterSelection
    psi = FilterSelection (
        'JPSI'   ,
        [ psi ] ,
        Code  = """
        in_range ( 2.990 * GeV , M , 3.210 * GeV)
        & CHILDCUT ( 1 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 2 , PROBNNmu>0.10 ) )
        & CHILDCUT ( 2 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 2 , PROBNNmu>0.10 ) )
        """
        )
    
    from StandardParticles   import StdAllNoPIDsMuons as muons 
    
    from PhysConf.Selections import RebuildSelection
    muons = RebuildSelection ( muons )
    
    from PhysConf.Selections import FilterSelection 
    muons =  FilterSelection (
        'MUONS'     ,
        [ muons ]   ,
        Code = """
        ISMUON & switch ( PROBNNmu < 0 , PIDmu > 2 , PROBNNmu>0.10 )
        """ ) 
    
    config = {
        'DecayDescriptor'      : 'chi_c1(1P) -> J/psi(1S) mu+ mu-'               ,
        'Combination12Cut'     : ' ( AM - AM1 ) < ( 3.63 * GeV - 3.096 * GeV ) ' ,
        'CombinationCut'       : 'in_range ( 3.44 * GeV , AM , 3.63 * GeV )'     ,
        'MotherCut'            : ' ( CHI2VXNDF < 10 ) & BPVVALID() '             ,
        'CheckOverlapTool'     : 'LoKi::CheckOverlap/CHECKOVERLAP:PUBLIC'        ,
        'InputPrimaryVertices' : 'Primary' , 
        'ReFitPVs'             : True 
        } 

    from PhysConf.Selections import Combine3BodySelection
    chic   = Combine3BodySelection ( 'Chic'        , ## unique name 
                                    [ psi, muons ] , ## required selections
                                    **config       ) ## algorithm properties
    
    if '2016' == the_year : 
        from PhysConf.Selections import MomentumScaling
        chic = MomentumScaling ( chic , Year = the_year , Turbo = 'PERSISTRECO' )    
        
    from PhysConf.Selections import PrintSelection
    chic = PrintSelection  ( chic )     

    b1 = BenderSelection (
        'CHIC'                           ,
        chic                             , 
        InputPrimaryVertices = 'Primary' , 
        )
    
    from PhysConf.Selections import SelectionSequence
    davinci.UserAlgorithms += [ SelectionSequence('SEQ:CHIC' , b1 ).sequence() ]    
    
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )

    ## (9) specific for persist reco
    from Configurables import DstConf, TurboConf
    DstConf   () .Turbo       = True
    TurboConf () .PersistReco = True
    TurboConf () .DataType    = the_year
    
    ## (10) fix for persist reco, not needed if "plain" Turbo is used 
    #   I woudl suggest to merge it into a single place, e.g. in TurboConf
    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc( Dump = True )
    from Configurables import Gaudi__DataLink as Link
    for  name , target , what  in [
        ( 'LinkHlt2Tracks' , '/Event/Turbo/Hlt2/TrackFitted/Long' , '/Event/Hlt2/TrackFitted/Long' ) , 
        ( 'LinkDAQ'        , '/Event/Turbo/DAQ'                   , '/Event/DAQ'                   ) ,
        ] :
        dod.AlgMap [ target ] = Link ( name , Target = target , What = what , RootInTES = '' ) 
        
    from BenderTools.Utils import silence
    silence()


    #
    ## come back to Bender
    #
    setData ( datafiles , catalogs , castor )
    
    #
    ## start Gaudi
    #
    gaudi = appMgr()
    
    alg1 = ChiC ( b1 )
    
    return SUCCESS 

# =============================================================================
# The actual job steering
if '__main__' == __name__ :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  )
    
    ## get-files-from-BK '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo03/94000000/LEPTONS.MDST' -g CERN -m 2
    inputdata = [
        '/lhcb/LHCb/Collision16/LEPTONS.MDST/00053752/0000/00053752_00000188_1.leptons.mdst',
        '/lhcb/LHCb/Collision16/LEPTONS.MDST/00053752/0000/00053752_00000330_1.leptons.mdst',
        ]
    configure ( inputdata , castor = True ,
                params = { 'Year' : '2016'} )
    
    run ( 10000 )
    
    gaudi = appMgr()

# =============================================================================
# The END 
# =============================================================================
