#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file Turbo.py 
#
#  Reading TURBO with Bedner 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2017-01-20
#
# =============================================================================
""" Reading TURBO with Bedner 

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2017-01-20" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.Main               import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.Turbo' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## @class Turbo
#  Reading TURBO with Bender 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class Turbo(Algo):
    """Reading TURBO with Bender 
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """
        The main 'analysis' method
        """
        
        ## get particles from the input locations 
        particles = self.select ( 'charm', '[D+ -> K+ pi- pi-]CC'  )
        if particles.empty() :
            return self.Warning('No input good D+ mesons', SUCCESS )
        
        tup = self.nTuple('MyTuple')
        
        fun_mass1  = M
        fun_mass2  = DTF_FUN      ( M , True ) 
        fun_chi2   = DTF_CHI2NDOF (     True )
        fun_ctau   = DTF_CTAU     ( 0 , True ) 

        for d in particles :

            chi2 = fun_chi2 ( d )
            if not 0<= chi2 < 10 : continue
            ctau = fun_ctau ( d )

            tup.column( 'm1'   , fun_mass1 ( d ) / GeV ) 
            tup.column( 'm2'   , fun_mass2 ( d ) / GeV ) 
            tup.column( 'ctau' , fun_ctau  ( d )       ) 
            tup.column( 'chi2' , chi2                  ) 
            
            self.treatKine    ( tup , d , '_D' )
            self.treatPions   ( tup , d )
            self.treatKaons   ( tup , d )
            self.treatTracks  ( tup , d )

            tup.write() 
            
        ## 
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    ## import DaVinci 
    from Configurables import DaVinci
    ## delegate the actual configuration to DaVinci
    rootInTES = '/Event/Turbo'
    the_line  = 'Hlt2CharmHadDpToKpPimPipTurbo/Particles'
    
## '/Event/Turbo/Hlt2CharmHadDpToKpPimPipTurbo/Particles'
    the_year  = params['Year']
    
    from PhysConf.Filters import LoKi_Filters
    fltrs = LoKi_Filters (
        VOID_Code = """
        0 < CONTAINS('%s/%s')
        """ % ( rootInTES , the_line ) 
        )
    
    dv = DaVinci ( DataType        = the_year                ,
                   InputType       = 'MDST'                  ,
                   Turbo           = True                    ,
                   RootInTES       = rootInTES               ,
                   ## EventPreFilters = fltrs.filters('WG')     ,   
                   TupleFile       = 'Turbo.root'        ,  ## IMPORTANT
                   )
    
    from PhysConf.Selections import AutomaticData
    input  = AutomaticData ( the_line )
    
    # =========================================================================
    ## insert momentum scaling
        
    from PhysConf.Selections import MomentumScaling 
    input = MomentumScaling ( input , Turbo = True , Year = the_year ) 
    
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )
    # =========================================================================

    ## Bender-selection - wrap Bender algorithm as ``Selection''
    bsel   = BenderSelection   ( 'Turbo', input )

    ## add Selecion sequence into DaVinci dataflow 
    dv.UserAlgorithms.append ( bsel ) 
    
    ## define the input data
    setData  ( inputdata , catalogs , castor )
    
    ## get/create application manager
    gaudi = appMgr() 
    
    ## (1) create the algorithm with given name 
    alg   = Turbo ( bsel )
             
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

    #
    ## job configuration
    #
    inputdata = [
        '/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000001_1.turbo.mdst'
        ### 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000001_1.turbo.mdst'
        ]
    configure( inputdata , castor = True , params = { 'Year' : '2015' } )
    
    ## event loop 
    run(5000)
    
    gaudi = appMgr()
    alg   = gaudi.algorithm('Turbo')
    alg.NTuplePrint = True 
                          
    
# =============================================================================
# The END
# =============================================================================


