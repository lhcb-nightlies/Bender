#!/usr/bin/env python
# =============================================================================
## @file BenderExample/PhiMC_uDST.py
# The simple Bender-based example: plot dikaon mass peak with MC-truth
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date 2006-10-12
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
# =============================================================================
"""
The simple Bender-based example plot dikaon mass peak with MC-truth

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the 
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
``No Vanya's lines are allowed in LHCb/Gaudi software.''
"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
__date__    = " 2006-10-12 "
__version__ = " Version $Revision$ "
# =============================================================================
## import everything form bender
from Bender.MainMC import *

# =============================================================================
## Simple class for access MC-truth 
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2006-10-13
class PhiMC(AlgoMC) :
    """
    Simple class to plot dikaon mass peak
    """
    ## standard constructor
    def __init__ ( self , name = 'PhiMC' , **args ) :
        """
        Standard constructor
        """ 
        AlgoMC.__init__ ( self , name , **args )

    ## standard method for analyses
    def analyse( self ) :
        """
        Standard method for analyses
        """
        
        mcphi   = self.mcselect ( 'mcPhi', 'phi(1020) => K+ K-')        
        if mcphi.empty() : return self.Warning('No MC-phi is found!', SUCCESS )
        
        matcher = self.mcTruth ()
        mc  = MCTRUTH( matcher , mcphi ) 
        
        ## select all kaons 
        kaons = self.select( 'kaons'  , ( 'K+'  == ABSID ) & mc )
        
        self.select( "K+" , kaons , 0 < Q )
        self.select( "K-" , kaons , 0 > Q )
        
        dikaon = self.loop( "K+ K-" , "phi(1020)" )
        for phi in dikaon :
            m12 = phi.mass(1,2) / 1000 
            if 1.1 < m12  : continue
            mass = M(phi)/1000
            if 0   > mass : continue 
            self.plot ( mass , 'K+ K- mass'           , 1. , 1.1 ) 
            chi2 = VCHI2( phi )
            if 0 > chi2 or 49 < chi2 : continue
            self.plot ( mass , 'K+ K- mass,0<chi2<49' , 1. , 1.1 )  
            if not mc ( phi ) : continue 
            self.plot ( mass , 'K+ K- mass,mctruth'   , 1. , 1.1 ) 
           
        self.setFilterPassed( True ) 
        return SUCCESS
    
# =============================================================================
def configure ( datafiles , catalogs  = [] , castor = False , params = {} ) :
    """
    Perform the real configuration of the job
    """
    
    #
    ## Static Configuration (``Configurables'')
    #    
    from Configurables import DaVinci
    daVinci = DaVinci (
        DataType      = '2016' ,
        Simulation    = True   ,
        InputType     = 'MDST' ,
        RootInTES     = '/Event/AllStreams'       ,
        HistogramFile = 'PhiMC_uDST_Histos.root'  , 
        )
    
    #
    ## temporary for DaVinci v42r3
    #
    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc( Dump = True )
    dod.NodeMap[ '/Event/AllStreams/pSim'] = 'DataObject'
    from Configurables import Gaudi__DataLink as Link
    for  name , target , what  in [
        ( 'LinkMC2Sim4Partices' , '/Event/AllStreams/pSim/MCParticles' , '/Event/AllStreams/pMC/Particles'   ) , 
        ( 'LinkMC2Sim4Vertices' , '/Event/AllStreams/pSim/MCVertices'  , '/Event/AllStreams/pMC/Vertices'    )
        ] : 
        dod.AlgMap [ target ] = Link ( name , Target = target , What = what , OutputLevel = 1 , PropertiesPrint = True )
        

    from StandardParticles import StdTightKaons

    phimc = BenderMCSelection (
        'PhiMC'                                         , ## unique name 
        [ StdTightKaons ]                               , ## input selections
        HistoPrint = True                               , ## print histos
        PP2MCs     = [ 'Relations/Rec/ProtoP/Charged' ] , ## MC-truth relation tables
        CollectP2MCPLinks = True                        ) ## autocollect P->MCP links

    daVinci.UserAlgorithms.append ( phimc ) 

    #
    ## define the input data
    #
    setData ( datafiles , catalogs , castor , useDBtags = True ) 

    
    #
    ## Dynamic Configuration: Jump into the wonderful world of GaudiPython 
    #
    
    ## get the actual application manager (create if needed)
    gaudi = appMgr() 
    
    ## create local algorithm:
    alg = PhiMC( phimc )
    
    return SUCCESS 
    
# =============================================================================
## job steering 
if __name__ == '__main__' :

    ## make printout of the own documentations 
    print '*'*120
    print                      __doc__
    print ' Author  : %s ' %   __author__    
    print ' Version : %s ' %   __version__
    print ' Date    : %s ' %   __date__
    print '*'*120  
  
    ## configure the job:
    inputdata = [
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000078_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000035_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000010_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000012_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000018_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000023_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000057_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000002_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000009_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000014_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000052_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000066_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000031_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000058_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000042_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000013_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000090_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000088_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000017_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000091_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000001_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000005_3.AllStreams.mdst',
        '/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000053_3.AllStreams.mdst',
      ]
    
    configure( inputdata , castor = True )
    
    run(10000) 

# =============================================================================
# The END 
# =============================================================================
