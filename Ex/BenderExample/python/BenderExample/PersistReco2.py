#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file PersistReco2.py 
#
#  Reading TURBO/PersistReco with Bender 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2017-01-20
#
# =============================================================================
""" Reading TURBO/PersistReco with Bedner 

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2017-01-20" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.Main               import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.Turbo' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## @class PersistsReco
#  Reading TURBO with Bender 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class PersistsReco(Algo):
    """Reading TURBO with Bender 
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """
        The main 'analysis' method
        """
        
        ## get particles from the input locations 
        Ds1    = self.select ( 'Ds1', '[D*_s+ -> D_s+ mu+ mu-]CC')
        if Ds1.empty() : return self.Warning('No input Ds*', SUCCESS )

        tup = self.nTuple('Ds1')
        for d in  Ds1 :
            tup.column_float ( 'm'   ,  M  (d) / GeV )
            tup.column_float ( 'm1'  ,  M1 (d) / GeV )
            tup.column_float ( 'm23' ,  M23(d) / GeV )
            tup.write() 
        ## 
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    
    ## import DaVinci 
    from Configurables import DaVinci
    
    the_year  = params['Year'] 
    
    rootInTES = '/Event/Turbo'
    the_line  = 'Hlt2CharmHadDspToKmKpPipTurbo/Particles'
    
    ## use pre-filters to speedup 
    from PhysConf.Filters import LoKi_Filters
    fltrs = LoKi_Filters (
        VOID_Code = """
        0 < CONTAINS('%s/%s')
        """ % ( rootInTES , the_line )
        )
    
    ## (1) delegate the actual configuration to DaVinci
    dv = DaVinci ( DataType        = the_year                ,
                   InputType       = 'MDST'                  ,
                   RootInTES       = rootInTES               ,
                   EventPreFilters = fltrs.filters('FILTER') ,   
                   TupleFile       = 'TurboDs1.root'         ,
                   Turbo           = True                      ## IMPORTANT 
                   )
    
    # =============================================================================
    # Stuff specific for Persist Reco, not needed for "plain" Turbo
    # =============================================================================
    
    ## (9) specific for persists reco
    from Configurables import DstConf, TurboConf
    DstConf   () .Turbo       = True
    TurboConf () .PersistReco = True
    TurboConf () .DataType    = the_year
    
    ## (10) fix for persist reco, not needed if "plain" Turbo is used 
    #   I woudl suggest to merge it into a single place, e.g. in TurboConf
    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc()
    from Configurables import Gaudi__DataLink as Link
    for  name , target , what  in [
        ( 'LinkHlt2Tracks' , '/Event/Turbo/Hlt2/TrackFitted/Long' , '/Event/Hlt2/TrackFitted/Long' ) , 
        ( 'LinkDAQ'        , '/Event/Turbo/DAQ'                   , '/Event/DAQ'                   ) ,
        ] : 
        dod.AlgMap [ target ] = Link ( name , Target = target , What = what , RootInTES = '' ) 
        
    # =============================================================================
    ## Start  selection
    # =============================================================================
    
    from PhysConf.Selections import AutomaticData
    Ds = AutomaticData( the_line )
    
    from StandardParticles   import StdAllNoPIDsMuons as muons
    from PhysConf.Selections import RebuildSelection
    muons = RebuildSelection ( muons ) 
    
    ## apply momentum scaling 
    from PhysConf.Selections import MomentumScaling 
    Ds    = MomentumScaling ( Ds , Turbo = 'PersistReco', Year = the_year )
    
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )
    
    from PhysConf.Selections import CombineSelection
    Ds1 = CombineSelection (
        'Ds12Dsmumu'   , ## the name 
        [ Ds , muons ] , ## input
        DecayDescriptors = [
        "[D*_s+ -> D_s+ mu+ mu-]cc",
        "[D*_s+ -> D_s+ mu+ mu+]cc",
        "[D*_s+ -> D_s+ mu- mu-]cc"
        ],
        CombinationCut  = "(AM -AM1 < 1200)", 
        MotherCut       = """
        ( M - M1     < 1000 ) & 
        ( CHI2VXNDOF <   20 ) 
        """,
        InputPrimaryVertices = 'Primary',
        CheckOverlapTool     = "LoKi::CheckOverlap",
        ReFitPVs             = True ,
        DaughtersCuts = {
        "mu+": " HASMUON & ISMUON & (TRGHOSTPROB<0.5) "
        }
        )
    
    ## debugging 
    from PhysConf.Selections import PrintSelection
    Ds1 = PrintSelection ( Ds1 )

    ## Bender-selection - wrap Bender algorithm as ``Selection''
    bsel   = BenderSelection   ( 'DS', [ Ds1 ] )
    
    ## add Selecion sequence into DaVinci dataflow 
    dv.UserAlgorithms.append ( bsel ) 
    
    ## define the input data
    setData  ( inputdata , catalogs , castor )
    
    ## get/create application manager
    gaudi = appMgr() 
    
    ## (1) create the algorithm with given name 
    alg   = PersistsReco ( bsel )
             
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

    #
    ## job configuration
    #
    inputdata = [
        '/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000001_1.turbo.mdst',
        '/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000002_1.turbo.mdst',
        ]
    configure( inputdata , castor = True , params = { 'Year' : '2015' } )
    
    ## event loop 
    run(5000)
    
    
# =============================================================================
# The END
# =============================================================================


