#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file MomScale.py 
#
#  Check Momentum scaling for Run-II data
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2017-01-20
#
# =============================================================================
"""Check Momentum scaling for Run-II data 

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2017-01-20" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.Main               import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderTutor.MomScale' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## @class CheckMass
#  Check Momentum scaling for Run-II data
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class CheckMass(Algo):
    """
    Check Momentum scaling for Run-II data
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """
        The main 'analysis' method
        """
        
        ## get particles from the input locations 
        particles = self.select ( 'bmesons', '[B+ -> J/psi(1S) K+]CC'  )
        if particles.empty() : return self.Warning('No input particles', SUCCESS )
        
        tup = self.nTuple('MyTuple')
        
        fun_mass1  = M
        fun_mass2  = DTF_FUN      ( M , True , 'J/psi(1S)' ) 
        fun_chi2   = DTF_CHI2NDOF (     True , 'J/psi(1S)' )
        fun_ctau   = DTF_CTAU     ( 0 , True , strings( ['J/psi(1S)'] ) ) 
        min_probmu = MINTREE ( 'mu+' == ABSID , PROBNNmu  )
        
        for b in particles :
            
            psi = b ( 1 ) ## the first daughter: J/psi 
            k   = b ( 2 )

            ## some very primitive selection

            ## 0) mass-window
            if not 5.2 <= M ( b ) / GeV   < 5.3 : continue
            
            ## 1) j/psi mas 
            if not 3.0 <  M ( psi ) / GeV < 3.2 : continue

            ## 2) pid for kaons and muons
            if PROBNNk( k ) < 0.5               : continue
            if min_probmu   < 0.5               : continue

            ## 3) good chi2(DTF) and c*tau :
            if not 0   <= fun_chi2 ( b )       <  5 : continue
            if not 0.4 <= fun_ctau ( b ) / mm  < 10 : continue 

            ## calculate the quantities 
            m1 = fun_mass1 ( b ) / GeV 
            m2 = fun_mass2 ( b ) / GeV 
            
            tup.column_float ( 'm1' , m1 )
            tup.column_float ( 'm2' , m2 )
            
            self.plot ( m1 , 'mass (no-fit)'  , 5.2 , 5.3 )
            self.plot ( m2 , 'mass (DTF-fit)' , 5.2 , 5.3 )
                        
            tup.write() 
            
        ## 
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    ## import DaVinci 
    from Configurables import DaVinci
    ## delegate the actual configuration to DaVinci
    rootInTES = '/Event/PSIX'
    the_line  = 'Phys/SelPsiKForPsiX/Particles'
    
    the_year  = params['Year']
    
    from PhysConf.Filters import LoKi_Filters
    fltrs = LoKi_Filters (
        VOID_Code = """
        0 < CONTAINS('%s/%s')
        """ % ( rootInTES , the_line ) 
        )
    
    dv = DaVinci ( DataType        = the_year                ,
                   InputType       = 'MDST'                  ,
                   RootInTES       = rootInTES               ,
                   EventPreFilters = fltrs.filters('WG')     ,   
                   TupleFile       = 'CheckMass.root'        ,  ## IMPORTANT 
                   HistogramFile   = 'CheckMass_histos.root'    ## IMPORTANT 
                   )
    
    from PhysConf.Selections import AutomaticData
    input  = AutomaticData ( the_line )
    
    # =========================================================================
    ## insert momentum scaling
    from PhysConf.Selections import MomentumScaling 
    input = MomentumScaling ( input ) 
    
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )
    # =========================================================================

    ## Bender-selection - wrap Bender algorithm as ``Selection''
    bsel   = BenderSelection   ( 'CheckMass', input )

    ## add Selecion sequence into DaVinci dataflow 
    dv.UserAlgorithms.append ( bsel ) 
    
    ## define the input data
    setData  ( inputdata , catalogs , castor )
    
    ## get/create application manager
    gaudi = appMgr() 
    
    ## (1) create the algorithm with given name 
    alg   = CheckMass ( bsel )
             
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

    #
    ## job configuration
    #
    
    ## BKQuery ( '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/WGBandQSelection7/90000000/PSIX.MDST'   )
    inputdata_12 = [
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000221_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000282_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000238_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000185_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000278_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000190_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000192_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000247_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000225_1.psix.mdst',
        '/lhcb/LHCb/Collision12/PSIX.MDST/00035290/0000/00035290_00000289_1.psix.mdst',
        ]
    
    ## 
    ## BKQuery ( '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/WGBandQSelection11/90000000/PSIX.MDST'   )
    inputdata_15 = [
        '/lhcb/LHCb/Collision15/PSIX.MDST/00051148/0000/00051148_00000071_1.psix.mdst',
        '/lhcb/LHCb/Collision15/PSIX.MDST/00051148/0000/00051148_00000121_1.psix.mdst',
        '/lhcb/LHCb/Collision15/PSIX.MDST/00051148/0000/00051148_00000029_1.psix.mdst',
        '/lhcb/LHCb/Collision15/PSIX.MDST/00051148/0000/00051148_00000023_1.psix.mdst',
        '/lhcb/LHCb/Collision15/PSIX.MDST/00051148/0000/00051148_00000038_1.psix.mdst',
        ]
    
    ## BKQuery ( '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping26/WGBandQSelection11/90000000/PSIX.MDST'   )
    inputdata_16 = [
        '/lhcb/LHCb/Collision16/PSIX.MDST/00054571/0000/00054571_00000334_1.psix.mdst',
        '/lhcb/LHCb/Collision16/PSIX.MDST/00054571/0000/00054571_00000363_1.psix.mdst',
        '/lhcb/LHCb/Collision16/PSIX.MDST/00054571/0000/00054571_00000356_1.psix.mdst',
        '/lhcb/LHCb/Collision16/PSIX.MDST/00054571/0000/00054571_00000046_1.psix.mdst',
        '/lhcb/LHCb/Collision16/PSIX.MDST/00054571/0000/00054571_00000258_1.psix.mdst'
        ]

    
    ##  configure( inputdata_12 , castor = True , params = { 'Year' : '2012' } )
    ## configure( inputdata_15 , castor = True , params = { 'Year' : '2015' } )
    configure( inputdata_16 , castor = True , params = { 'Year' : '2016' } )
    
    ## event loop 
    run(5000)
    
    gaudi = appMgr()
    alg   = gaudi.algorithm('CheckMass')
    alg.NTuplePrint = True 
                          
    
# =============================================================================
# The END
# =============================================================================


