#!/bin/bash 
# -*- coding: utf-8 -*-
# =============================================================================
## @file eos_ls
#
#  Trivial Bender-based script to check the presence of files in EOS 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
#  @date   2011-10-25
#  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
#
# =============================================================================
## if  ( -r  /afs/cern.ch/project/eos/installation/client/etc/setup.csh ) then
##    source /afs/cern.ch/project/eos/installation/client/etc/setup.csh
##else 
##    echo 'No eos afs setup available!' > /dev/stderr 
##    exit 1 
##endif

## needed to expand aliases for bash 
shopt -s expand_aliases

##type eos > /dev/null 2>&1
##if [[ $? -eq 1 ]]; then
if [[ -r /afs/cern.ch/project/eos/installation/client/etc/setup.sh ]]; then
    source /afs/cern.ch/project/eos/installation/client/etc/setup.sh
fi
##fi

type eos > /dev/null 2>&1
if [[ $? -eq 1 ]]; then
    echo 'No EOS accessible' >&2
    exit 1
fi

EOS_CMD_TMP=$(alias eos)
if [[ $? -eq 1 ]]; then
    eos ls -al "$@"
    exit 0 
fi
EOS_CMD_TMP=${EOS_CMD_TMP#*=}
EOS_CMD_TMP=${EOS_CMD_TMP/\'/}
EOS_CMD=${EOS_CMD_TMP/\'/}

$EOS_CMD ls -la "$@"

# =============================================================================
# The END 
# =============================================================================
