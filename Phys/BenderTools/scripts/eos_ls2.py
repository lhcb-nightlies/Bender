#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file eos_ls.py
#
#  Simple script to check file at (CERN)eos 
#  actually it mimics 'eos ls' command using ROOT access
# 
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
# @author VanyaBELYAEV Ivan.Belyaev@itep.ru
# @date   2016-06-02
#
# =============================================================================
"""Simple script to check fiel at (CERN)eos 
   actually it mimics 'eos ls' command using ROOT access

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the 
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2016-06-02"
__version__ = "$Revision: 201763 $"
__all__     = ( 'accessURLs' , ) 
# =============================================================================
import ROOT,sys,os,logging  
ROOT.PyConfig.IgnoreCommandLineOptions = True

# =============================================================================
class Mute(object):
    ## class variables: dev-null device & instance counter 
    _devnull = 0
    _cnt     = 0    
    def __init__( self , out = True , err = False ):
        
        self._out = out
        self._err = err

        # increment instance counter 
        self.__class__._cnt += 1

        # create dev-null if not done yet 
        if not self.__class__._devnull :
            self.__class__._devnull = os.open ( os.devnull , os.O_WRONLY )            

    def __del__  ( self ) :
        # decrement instance counter 
        self.__class__._cnt -= 1        
        # close dev-null if not done yet 
        if self.__class__._cnt <= 0 and self.__class__._devnull : 
            os.close ( self.__class__._devnull  )
            self.__class__._devnull = 0
            
    ## context-manager 
    def __enter__(self):
        ## Save the actual stdout (1) and stderr (2) file descriptors.
        self.save_fds =  os.dup(1), os.dup(2)  # leak was here !!!
        ## mute it!
        if self._out : os.dup2 ( self.__class__._devnull , 1 )  ## C/C++
        if self._err : os.dup2 ( self.__class__._devnull , 2 )  ## C/C++

        return self
    
    ## context-manager 
    def __exit__(self, *_):
        # Re-assign the real stdout/stderr back to (1) and (2)  (C/C++)
        if self._err : os.dup2 ( self.save_fds[1] , 2 )
        if self._out : os.dup2 ( self.save_fds[0] , 1 )        
        # fix the  file descriptor leak
        # (there were no such line in example, and it causes
        #      the sad:  "IOError: [Errno 24] Too many open files"        
        os.close ( self.save_fds[1] ) 
        os.close ( self.save_fds[0] )

# =========================================================================
## make parser: 
# =========================================================================
import argparse 
parser = argparse.ArgumentParser(
    prog        = 'eos_ls2' ,
    description = """
    Helper module to get Grid access-URL for given LFN.
    [Actually it is wrapper over dirac-dms-lfn-accessURL script]
    """
    )
parser.add_argument (
    "files"        ,
    nargs    = '+' ,
    help     = "files to be checked" )

group = parser.add_mutually_exclusive_group()
group.add_argument (
    '-v'                           ,
    '--verbose'                    ,
    action  = "store_true"         ,
    dest    = 'Verbose'            ,
    help    = "``Verbose'' processing [default : %(default)s ]"  , 
    default = False 
    )

group.add_argument (
    '-q'                          ,
    '--quiet'                     ,
    action  = "store_false"       ,
    dest    = 'Verbose'           ,
    help    = "``Quiet'' processing [default : %(default)s ]"  
    )


# =============================================================================
if '__main__' == __name__ :
    
    ## use parser:
    args = [ a for a in sys.argv[1:] if '--' != a ]
    
    config  = parser.parse_args ( args ) 
    silent  = not config.Verbose
    result  = {}
    
    if silent :
        ROOT.gROOT.ProcessLine("gErrorIgnoreLevel= %d ; " % (  ROOT.kError + 1  ) ) 
        
    for f in config.files :
        
            _f = ROOT.TFile.Open('root://eoslhcb.cern.ch/%s' %f , 'READ')
            if _f : print f
            
        
# =============================================================================
# The END 
# =============================================================================
