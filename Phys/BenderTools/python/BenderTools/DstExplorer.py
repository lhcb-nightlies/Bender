#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
## @file BenderTools/DstExplorer.py
#
#  Trivial Bender-based script to explore the content of (x,mu,s,r,...)DSTs
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
# Usage:
# @code
#     bender [options] file1 [ file2 [ file3 [ file4 ....'
# @endcode 
#
# Examples of actions at the prompt:
#
# - Exploring (r,s,m,fm)DSTs:
# @code 
# ls('/Event')
# ls('/Event/Charm')
# ls('/Event/Charm/Phys')
# ls('/Event/Charm/Phys/D2KKK')
# @endcode
# - Getting objects from TES (and loop over container):
# @code 
# Ds = get('/Event/Charm/Phys/D2KKK')
# for D in Ds : print D.decay()
#
# beauty = get('/Event/MC/MCParticles' , BEAUTY) ## use embedded filter!
# for b in beauty : print b.decay()
# @endcode
# - Go to the next event:
# @code
# run(1)             ## ditto: next 
# ls('/Event')
# @endcode
# - Run over several events 
# @code
# run(100)          ## smimilar: run_progress  - the sam ebut with progress-bar
# ls('/Event')
# @endcode
# - Skip certain events
# @code
# skip(40)
# @endcode
# - Rewind to the start of the input file
# @code
# rewind()
# run(1)
# @endcode
# - Loop over events in search for ``valid'' data
# @code
# Ds, evt = seekForData('/Event/Charm/Phys/DstarForPromptCharm/Particles' )
# @endcode
# - Loop in search for event with certain Stripping decision:
# @code
# dec, evt = seekStripDecision('.*Lam.*' )
# @endcode
# - Loop in search for events with certaint Hlt1 decision: 
# @code
# dec, evt = seekHlt1Decision('Hlt1.*DiMuon.*' )
# @endcode
# - Loop in search for events with certaint Hlt2 decision: 
# @code
# dec, evt = seekHlt2Decision('Hlt2.*JPsi.*' )
# @endcode
# - Loop in search for events with certaint L0-criterion
# @code
# criterion = L0_CHANNEL_RE('.*Muon.*')
# l0du , evt = seekL0Decision( criterion )
# @endcode
# - Loop in search for events from  run-event list
# @code
# the_list = [  (1,341) , (5,44234) , (3245,2142) ]
# dec, evt = seekRunEvent( the_list )
# @endcode
# - Loop in search for event with ODIN decision, e.g. with the given TCK:
# @code
# criterion = ODIN_TCK == xxx
# odin, evt = seekForODIN ( criterion )
# @endcode
# - Loop over ``good'' events, e.g. events with at least two good selected J/psi
# @code
# good_event = lambda: 2<= len(get('/Event/PSIX/Phys/SelDetachedPsisForBandQ/Particles'))
# for i in irun ( 1000 , good_events ) :
# ...         hdr = get('/Event/Rec/Header')
# ...         print hdr.runNumber(), hdr.eventNumber()
# @endcode
#
# =============================================================================
"""Trivial Bender-based script to explore the content of (x,mu,s,r,...)DSTs

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    
                                                                 
This file is a part of BENDER project:

  ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
 
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign of Dr.O.Callot et al.:

   ``No Vanya's lines are allowed in LHCb/Gaudi software''

Examples of action at the prompt: 

    - Exploring (r,s,m,fm)DSTs:
    
       >>> ls('/Event')
       >>> ls('/Event/Charm')
       >>> ls('/Event/Charm/Phys')
       >>> ls('/Event/Charm/Phys/D2KKK')

    - Getting objects from TES (and loop over container):
 
       >>> Ds = get('/Event/Charm/Phys/D2KKK')
       >>> for D in Ds : print D.decay()

       >>> beauty = get('/Event/MC/MCParticles' , BEAUTY) ## use embedded filter! 
       >>> for b in beauty : print b.decay()
       
    - Go to the next event:

       >>> run(1)             ## ditto: next 
       >>> ls('/Event')

    - Run over several events 

       >>> run(100)          ## smimilar: run_progress  - the sam ebut with progress-bar
       >>> ls('/Event')

    - Skip certain events

       >>> skip(40)
  
    - Rewind to the start of the input file

       >>> rewind()
       >>> run(1)   

    - Loop over events in search for ``valid'' data

       >>> Ds, evt = seekForData('/Event/Charm/Phys/DstarForPromptCharm/Particles' )

    - Loop in search for event with certain Stripping decision:

       >>> dec, evt = seekStripDecision('.*Lam.*' )

    - Loop in search for events with certain Hlt1 decision: 
 
       >>> dec, evt = seekHlt1Decision('Hlt1.*DiMuon.*' )

    - Loop in search for events with certain Hlt2 decision: 
 
       >>> dec, evt = seekHlt2Decision('Hlt2.*JPsi.*' )

    - Loop in search for events with certain L0-criterion 

       >>> criterion = L0_CHANNEL_RE('.*Muon.*')
       >>> l0du, evt = seekL0Decision( criterion )

    - Loop in search for events from  run-event list
 
       >>> the_list = [  (1,341) , (5,44234) , (3245,2142) ]
       >>> dec, evt = seekRunEvent( the_list )

    - Loop in search for event with some ODIN decision, e.g.the given TCK:
    
        >>> criterion = ODIN_TCK == xxx
        >>> odin, evt = seekForODIN ( criterion )
        
    - Loop over ``good'' events, e.g. events with at least two good selected J/psi

        >>> good_event = lambda: 2<= len(get('/Event/PSIX/Phys/SelDetachedPsisForBandQ/Particles'))
        >>> for i in irun ( 1000 , good_event ) :
        ...         hdr = get('/Event/Rec/Header')
        ...         print hdr.runNumber(), hdr.eventNumber()
        
"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__    = "2010-09-10"
__version__ = '$Revision$'
__all__     = ( 'configure',  ) 
__usage__   = 'dst_explorer [options] file1 [ file2 [ file3 [ file4 ....'
# =============================================================================
# logging 
# =============================================================================
from Bender.Logger import getLogger, setLogging, isatty 
if '__main__' == __name__ : logger = getLogger ( 'BenderTools.DstExplorer' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## configure the application from parser data  
def configure ( config , colors = False ) :
    """Configure the application from parser data 
    """
        
    #
    if config.OutputLevel <= 3 and not config.Quiet :
        _vars   = vars ( config )
        _keys   = _vars.keys()
        _keys .sort()
        logger.info ( 'Configuration:')
        for _k in _keys : logger.info ( '  %15s : %-s ' % ( _k , _vars[_k] ) )
    
    ## redefine output level for 'quiet'-mode
    if config.OutputLevel > 5 :
        config.OutputLevel = 5
        logger.info('set OutputLevel to be %s ' % config.OutputLevel )
        
    if config.OutputLevel < 0 :
        config.OutputLevel = 0
        logger.info('set OutputLevel to be %s ' % config.OutputLevel )
        
    if config.OutputLevel <=2 : 
        from BenderTools.Utils import silence
        silence ( [ 'RootCnvSvc'    ,
                    'IODataManager' ,
                    'XmlParserSvc'  ,
                    'XmlCnvSvc'     ,
                    'XmlCatalogSvc' ,               
                    'XmlGenericCnv' ] )

    if config.Quiet and 4 > config.OutputLevel :
        config.OutputLevel = 4
        logger.info('set OutputLevel to be %s ' % config.OutputLevel ) 
        from BenderTools.Utils import silence, totalSilence
        silence      ()
        totalSilence ()

    #
    ## use coherent C++/Python logging levels 
    setLogging ( config.OutputLevel )
    
    #
    # some sanity actions:
    #
    config.RootInTES = config.RootInTES.strip()
    config.files     = [ i.strip() for i in config.files if i.strip() ]
    #
    ## start the actual action:
    #

    from Configurables import DaVinci
    #
    ## get the file type for the file extension
    #
    from BenderTools.Parser import dataType, hasInFile 
    pyfiles = [ i for i in config.files if  i.endswith('.py') ]
    files   = [ i for i in config.files if  i not in pyfiles  ]

    from BenderTools.Parser import fileList
    for f in config.FileList : files  += fileList ( f )

    if not files and not config.ImportOptions : 
        raise AttributeError('No data files are specified!')
    
    ## get some info from file names/extensision
    dtype, simu, ext = None,None,None
    ## try to get information form input file names (via command line) 
    if  files :
        dtype, simu, ext = dataType ( files )
        if not simu :
            from BenderTools.Parser import hasInFile 
            if   'DST'  == ext and hasInFile ( files , 'AllStreams.dst'  ) :
                simu = True 
                logger.info ("Simulation flag is activated 'ALLSTREAMS.DST'" )
            elif 'LDST' == ext and hasInFile ( files , 'AllStreams.ldst' ) :
                simu = True 
                logger.info ("Simulation flag is activated 'ALLSTREAMS.LDST'")
            elif 'MDST' == ext and hasInFile ( files , 'AllStreams.mdst' ) :
                simu = True
                logger.info ("Simulation flag is activated 'ALLSTREAMS.MDST'")
        logger.debug('DataType,Simu&extension:"%s",%s,"%s" (from files)' %
                    ( dtype , simu  , ext ) )
    ## try to get information from loaded options 
    elif config.ImportOptions :
        from Bender.DataUtils import evtSelInput
        ifiles = evtSelInput ( config.ImportOptions ) 
        dtype, simu, ext = dataType ( ifiles )
        if not simu :
            from BenderTools.Parser import hasInFile 
            if   'DST'  == ext and hasInFile ( files , 'AllStreams.dst'  ) :
                simu = True 
                logger.info ("Simulation flag is activated 'ALLSTREAMS.DST'")
            elif 'LDST' == ext and hasInFile ( files , 'AllStreams.ldst'  ) :
                simu = True 
                logger.info ("Simulation flag is activated 'ALLSTREAMS.LDST'")
            elif 'MDST' == ext and hasInFile ( files , 'AllStreams.mdst' ) :
                simu = True
                logger.info ("Simulation flag is activated 'ALLSTREAMS.MDST'")
            elif 'SIM' ==  ext :
                simu = True
                logger.info ("Simulation flag is activated '*.SIM'")
                
        logger.debug('DataType,Simu&extension:"%s",%s&"%s" (from EventSelector)' %
                         ( dtype , simu  , ext ) )

    if '2013' == dtype :
        logger.info ('Data type 2013 is redefined to be 2012')
        dtype = '2012'
        
    # 
    if ext.upper() in ( 'GEN'  , 'XGEN' , 'LDST' ) and not simu : simu = True 
    
    if dtype and dtype != config.DataType :
        logger.info ( 'Redefine DataType from  %s to %s '   % ( config.DataType, dtype ) )
        config.DataType  = dtype
        
    if simu and not config.Simulation : 
        logger.info ( 'Redefine Simulation from  %s to %s ' % ( config.Simulation, simu ) )
        config.Simulation  = simu 
        
    if config.Simulation and config.Lumi :
        logger.info('suppress Lumi for Simulated data')    
        config.Lumi = False

    ## summary information (when available) 
    from Configurables import LHCbApp
    LHCbApp().XMLSummary = 'summary.xml'

    daVinci = DaVinci (
        DataType    = config.DataType    ,
        Simulation  = config.Simulation  ,
        Lumi        = config.Lumi        ,  
        )
    
    if hasattr ( config , 'TupleFile' ) and config.TupleFile :
        logger.info ( 'Define TupleFile to be %s' % config.TupleFile )
        daVinci.TupleFile = config.TupleFile
        
    if hasattr ( config , 'HistoFile' ) and config.HistoFile :
        logger.info ( 'Define HistogramFile to be %s' % config.HistoFile )
        daVinci.HistogramFile = config.HistoFile 

    if config.MicroDST or 'mdst' == ext or 'MDST' == ext or 'uDST' == ext :
        logger.info ( 'Define input type as micro-DST' )
        daVinci.InputType = 'MDST'
        
    #
    ## try to guess RootInTES
    #
    turbo = None  
    from BenderTools.Parser import hasInFile 
    if   not config.RootInTES and hasInFile ( files , '.MDST' ) :
        mdst_streams = {
            #
            ## Standard stripping MDST-streams 
            'CHARM.MDST'              : '/Event/Charm'      ,
            'LEPTONIC.MDST'           : '/Event/Leptonic'   ,
            'BHADRON.MDST'            : '/Event/Bhadron'    ,
            'PID.MDST'                : '/Event/PID'        ,
            #
            ## Simulation 
            'ALLSTREAMS.MDST'         : '/Event/AllStreams' ,
            #
            ## B&Q WG-production 
            'PSIX.MDST'               : '/Event/PSIX'       ,
            'PSIX0.MDST'              : '/Event/PSIX0'      ,
            'BOTTOM.MDST'             : '/Event/BOTTOM'     ,
            'CHIC.MDST'               : '/Event/Chic'       ,
            'CHIC_TURBO.MDST'         : '/Event/Turbo'      ,
            #
            ## TURBO/2015
            'TURBO.MDST'              : '/Event/Turbo'      ,
            #
            ## TURBO/2016
            'CHARMCHARGED.MDST'       : '/Event/Turbo'      ,
            'CHARMKSHH.MDST'          : '/Event/Turbo'      ,
            'CHARMMULTIBODY.MDST'     : '/Event/Turbo'      ,
            'CHARMTWOBODY.MDST'       : '/Event/Turbo'      ,
            'CHARMSPECPARKED.MDST'    : '/Event/Turbo'      ,
            'CHARMSPECPRESCALED.MDST' : '/Event/Turbo'      ,            
            'LEPTONS.MDST'            : '/Event/Turbo'      ,
            #
            ## TURBO/2017             ,
            'CHARMCHARGED.MDST'       : '/Event/Turbo'      ,
            'CHARMMULTIBODY.MDST'     : '/Event/Turbo'      ,
            'CHARMSPEC.MDST'          : '/Event/Turbo'      ,
            'CHARMTWOBODY.MDST'       : '/Event/Turbo'      ,
            'LEPTONS.MDST'            : '/Event/Turbo'      ,            
            }
        
        streams = mdst_streams.keys()
        streams.sort( key = lambda  s: (len(s),s) )
        streams = reversed (  streams ) 
        
        stream = hasInFile ( files , streams )
        if stream :
            rootit           = mdst_streams[stream]
            ## attention, special action for Turbo-2107
            if 'Turbo' in rootit and '2017' == dtype :
                s,d,e = stream.partition('.')
                if s and d :
                    s = s[0].upper() + s[1:].lower()
                    rootit = '/Event/%s/Turbo' % s
            config.RootInTES = rootit 
            logger.info ('RootInTES is set to "%s" according to %s' % ( rootit , stream ) )            
            daVinci.InputType = 'MDST'
            if rootit.endswith('/Turbo') and not config.Turbo :
                logger.info ('Turbo flag  is set to True according to %s for %s' % ( rootit , stream ) )                            
                config.Turbo = True                 
        else :
            logger.warning ('MDST type and RootInTES are not auto-recognized')
            
    if config.Turbo :

        if not 'Turbo' in config.RootInTES :
            logger.warning ( 'Suspicios    RootInTES %s for TURBO input' % config.RootInTES  )
        if 'MDST' != daVinci.InputType     :
            logger.warning ( 'Inconsistent InputType %s for TURBO input' % daVinci.InputType )
                
        daVinci.Turbo =  True 
    
    if config.RootInTES and  0  != config.RootInTES.find ( '/Event' ) :
        config.RootInTES  = '/Event/' + config.RootInTES        
    if config.RootInTES and '/' == config.RootInTES[-1] :
        config.RootInTES  = config.RootInTES[:-1]        
    if config.RootInTES and '/Event' != config.RootInTES  : 
        daVinci.RootInTES = config.RootInTES
    #
    ## check for Grid-access
    #
    if config.Grid : 
        from Bender.DataUtils import hasGridProxy
        if not hasGridProxy () :
            logger.warning ( 'GRID proxy is not available, switch off GRID-lookup' )
            config.Grid = ''  ## SWITCH OFF Grid-lookup


    #
    ## DB tags
    #
    tags_set = False 
    if config.DDDBtag and config.CondDBtag :
        if not config.NoTagsFromDATA :
            config.NoTagsFromDATA  = True 
            logger.debug ('DDDB/CONDDB Tags are given explicitely, disable extraction from data')
        from Configurables import DaVinci
        dv = DaVinci ( DDDBtag   = config.DDDBtag   ,
                       CondDBtag = config.CondDBtag )
        tags_set = True 
        logger.info ('Use DDDBtag   : %s ' % dv.DDDBtag   )
        logger.info ('Use CONDDBtag : %s ' % dv.CondDBtag )
    elif config.DDDBtag    :
        logger.warning ('DDDB   tag is specified but CondDB tag it not specified!')
        from Configurables import DaVinci
        dv = DaVinci ( DDDBtag   = config.DDDBtag   )
        logger.info ('Use DDDBtag   : %s ' % dv.DDDBtag   )
    elif  config.CondDBtag :
        logger.warning ('CondDB tag is specified but DDDB   tag it not specified!')
        from Configurables import DaVinci
        dv = DaVinci ( CondDBtag = config.CondDBtag )
        logger.info ('Use CONDDBtag : %s ' % dv.CondDBtag )

    #
    ## try to use the latest available tags, if not specified from the command line 
    #
    latest = False 
    if not tags_set          and \
       not config.Simulation and \
       not config.DDDBtag    and \
       not config.CondDBtag  and config.DataType in ( '2010' ,
                                                      '2011' ,
                                                      '2012' ,
                                                      '2013' , 
                                                      '2015' ,
                                                      '2016' ,
                                                      '2017' ) :
        from Configurables import CondDB    
        CondDB( LatestGlobalTagByDataType = config.DataType ) 
        logger.info('Use latest global tag for data type %s' % config.DataType )
        latest = True

    if 'UPGRADE' == config.DataType.upper() :
        from Configurables import CondDB    
        db = CondDB( Upgrade = True )
        logger.info('Use Upgrade partition of CondDB')
        
        
    ## use tags from data? 
    use_dbtags = config.Simulation and not tags_set and not config.NoTagsFromDATA
    if ( tags_set or config.Simulation ) and not use_dbtags :
        from BenderTools.GetDBtags import print_conddb
        print_conddb()         

    if not tags_set and config.Simulation and config.NoTagsFromDATA :
        logger.info ("No DDDB/(SIM)CONDDB tags will be picked up from DATA") 
            
    if config.IgnoreDQFlags :
        logger.info('DataQuality flags will be ignored')
        daVinci.IgnoreDQFlags = config.IgnoreDQFlags
        
    ## specific action for (x)gen files 
    if   ext in ( 'gen' , 'xgen' , 'GEN' , 'XGEN' ) :
        from BenderTools.GenFiles import genAction
        genAction ( ext )  
    elif ext in ( 'sim' , 'SIM' ) :
        from BenderTools.GenFiles import simAction
        simAction ( ext )
            
        
    ## prepare to copy good/marked/tagged evenst
    if hasattr ( config , 'OutputFile' ) and config.OutputFile :
        from BenderTools.GoodEvents import copyGoodEvents
        if 0 <= config.OutputFile.find ( '.' ) : 
            copyGoodEvents (             config.OutputFile         ) 
        else :
            copyGoodEvents ( "%s.%s" % ( config.OutputFile , ext ) ) 

    ##  OutptuLevel
    from Configurables import MessageSvc
    msgSvc = MessageSvc ( OutputLevel = config.OutputLevel ) 
    
    ## import options (if specified) 
    for i in config.ImportOptions :
        logger.info ("Import options from file %s'" % i )
        from Gaudi.Configuration import importOptions
        importOptions ( i )  

    if colors and isatty() :
        
        logger.debug( 'Add colorization to MessageSvc' )
        
        from Configurables import MessageSvc
        msgsvc = MessageSvc (
            useColors        = True ,
            errorColorCode   = [ 'yellow' , 'red'  ] ,
            warningColorCode = [ 'red'             ] ,
            fatalColorCode   = [ 'blue'   , 'red'  ] ,
            )
            
        def _color_pre_start_action_ () :
            
            logger.debug( 'Add colorization to MessageSvc' )
            from GaudiPython.Bindings import AppMgr
            _g = AppMgr()
            if not _g : return 
            _s = _g.service('MessageSvc')
            if not _s : return 
            _s.useColors = True
            _s.errorColorCode   = [ 'yellow' , 'red'  ] 
            _s.warningColorCode = [ 'red'             ] 
            _s.fatalColorCode   = [ 'blue'   , 'red'  ] 
            ##_s.alwaysColorCode  = [ 'blue'            ] 
            ##_s.infoColorCode    = [ 'green'           ] 
            del _g, _s
            
        from Bender.Utils import addPreInitAction
        addPreInitAction ( _color_pre_start_action_ )

    else :
        
        from Configurables import MessageSvc
        msgsvc = MessageSvc ( useColors = False )
    
    ## set input data
    from Bender.Utils import setData

    ## import GaudiPython.Bindings
    
    setData ( files     = files                ,
              catalogs  = config.XmlCatalogs   ,  ## XML-catalogues 
              castor    = config.Castor        ,  ## use Castor/EOS lookup 
              grid      = config.Grid          ,  ## Use GRID to locate files
              useDBtags = use_dbtags           )
    
    return pyfiles


# =============================================================================
## Reset all DaVinci sequences (if needed)
def resetDaVinci() :
    """Reset all DaVinci sequences
    """
    def _action ( ) :
        """Reset all DaVinci sequences
        """
        from Gaudi.Configuration import allConfigurables 
        from Gaudi.Configuration import getConfigurable 
        for seq in ( 'DaVinciInitSeq'      ,
                     'DaVinciMainSequence' ,
                     'DaVinciSequence'     ,
                     'MonitoringSequence'  ,
                     'FilteredEventSeq'    ) :

            if not seq in allConfigurables : continue 
            cSeq = getConfigurable( seq )
            if cSeq and hasattr ( cSeq , 'Members' ) :
                logger.info ( 'Reset the sequence %s' % cSeq.name() )
                cSeq.Members = []

            ## reset the list of top-level algorithms 
            from Configurables import ApplicationMgr
            a = ApplicationMgr()
            a.TopAlg = []
            a.OutputLevel = options.OutputLevel
            
            from Configurables import MessageSvc
            m = MessageSvc ( OutputLevel = options.OutputLevel )
            
            from GaudiConf import IOHelper
            ioh = IOHelper () 
            ioh.setupServices()
            
    ## comment it out... Hm...
    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction ( _action )

# =============================================================================
if '__main__' == __name__ :
    
    logger.info ( 100*'*')
    logger.info ( ' Author  : %s ' % __author__   ) 
    logger.info ( ' Version : %s ' % __version__  ) 
    logger.info ( ' Date    : %s ' % __date__     )
    
# =============================================================================
# The END 
# =============================================================================

