################################################################################
# Package: Bender
################################################################################
gaudi_subdir(Bender v31r0)

gaudi_depends_on_subdirs(GaudiPython
                         Phys/AnalysisPython
                         Phys/LoKiAlgoMC
                         Phys/DaVinci)

gaudi_install_python_modules()
gaudi_install_scripts()

add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/BenderCustomConfs.confdb
    COMMAND echo "Bender.Configuration Bender BenderAlgo"    > ${CMAKE_CURRENT_BINARY_DIR}/BenderCustomConfs.confdb
    COMMAND echo "Bender.Configuration Bender BenderAlgoMC" >> ${CMAKE_CURRENT_BINARY_DIR}/BenderCustomConfs.confdb
)

add_custom_target(BenderCustomConfDB DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/BenderCustomConfs.confdb)

gaudi_merge_files_append(ConfDB BenderCustomConfDB ${CMAKE_CURRENT_BINARY_DIR}/BenderCustomConfs.confdb)
